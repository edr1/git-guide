This repository is meant to be used by:

[git and git-flow, a guide](https://medium.com/gumgum-tech/git-and-git-flow-a-guide-871d46a0ebcb) and [git and git flow, part 2](https://medium.com/@edrpls/git-and-git-flow-part-2-8de67ba2dd31).

The aim for this guide to be enough for you to work comfortably with git and git-flow.